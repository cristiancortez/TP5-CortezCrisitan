package ar.edu.unju.facultades.manager;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import ar.edu.unju.facultades.modelo.Alumno;


@ManagedBean
@SessionScoped
public class ManagerAlumno {
	private static List<Alumno> clienteList;
	static{	
		clienteList = new ArrayList<Alumno>();
		clienteList.add(new Alumno(1, "Nombre1", "Apellido1", "Domicilio1", 11111, "email1"));
		clienteList.add(new Alumno(2, "Nombre2", "Apellido2", "Domicilio2", 22222, "email2"));
		clienteList.add(new Alumno(3, "Nombre3", "Apellido3", "Domicilio3", 33333, "email3"));

	}
//___________________________________________	
	public static  List<Alumno>  list()
	{
		return clienteList;
	}
//___________________________________________	
	public static List<Alumno> buscarCliente(String nombreC,Integer documentoC) {
		List<Alumno>  listaSearchC = new ArrayList<Alumno>();
		int pas = 0;
		System.out.println(documentoC);
		System.out.println(nombreC);
		for (Alumno u : clienteList)
		{
				if (documentoC.equals(u.getDni())|| (u.getNombre().equalsIgnoreCase(nombreC)) )
				{
					listaSearchC.add(u);
					pas = 1;
					System.out.println(documentoC + pas );
					System.out.println(nombreC + pas );
					System.out.println(u);
					return listaSearchC;
				}  		
		}	
		if (pas== 0)
		{
				listaSearchC = ManagerAlumno.list();
		}
		return listaSearchC;
	}
	
//_________________________________________________________________
	public static int getIndex(Integer id)
	{
		for (int i = 0; i < clienteList.size(); i++) 
		{
			Alumno cliente = clienteList.get(i);
			if (cliente.getUsuarioId() == id)
			{
				return i;
			}		
		}
		return -1;
	}
//_________________________________________________________________
	public static int getById(Integer id)
	{
		for (int i = 0; i < clienteList.size(); i++)
		{
			Alumno cliente = clienteList.get(i);
			if (cliente.getUsuarioId() == id)
			{
				return i;
			}
		}
		return -1;
	}		
//_________________________________________________________________
	public static void actualizar(Alumno cliente)
	{
		clienteList.set(getIndex(cliente.getUsuarioId()), cliente);		
	}		
//__________________________________________________________________	
	public static void guardar(Alumno cliente)
	{		
		    clienteList .add(cliente);
			for (Alumno cliente1: clienteList)
			{
				System.out.println(cliente1.getNombre());
			}
    }
//________________________________________________________________	
	public static void ver(Alumno cliente)
	{		
		for (Alumno cliente0: clienteList)
		{
			System.out.println(cliente0);
		}
	}
	public static void eliminar(Alumno cliente) 
	{
		clienteList.remove(cliente);
	}	
	
}	
package ar.edu.unju.facultades.manager;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import ar.edu.unju.facultades.modelo.Usuario;
@ManagedBean
@SessionScoped
public class ManagerUsuario {
	private static List<Usuario> usuarioList;
	static{
			usuarioList = new ArrayList<Usuario>();
			//-----------------------id-nombre-clave-dni-correo---------------
			usuarioList.add(new Usuario(100, "Cristian", "123", 37508062, "emafreanrojas@hotmail.com"));
		
	}
	//_______________________________________________________________
	public static  List<Usuario>  list(){
		return usuarioList;
	}
	//________________________BUSQUEDA NUEVA
	public static List<Usuario> buscarNombre(String nombre,Integer documento) {
		List<Usuario>  listaSearch = new ArrayList<Usuario>();
		int pas = 0;
		System.out.println(documento);
		System.out.println(nombre);
		for (Usuario u : usuarioList) {
				if (documento.equals(u.getDni())|| (u.getNombre().equalsIgnoreCase(nombre)) ) {
					listaSearch.add(u);
					pas = 1;
					System.out.println(documento + pas );
					System.out.println(nombre + pas );
					System.out.println(u);
					return listaSearch;
				}  		
		}	
		if (pas== 0){
				listaSearch = ManagerUsuario.list();
		}
		return listaSearch;
	}	
	//_____________Valida CLAVE Y DNI_______________________________
	public static Usuario getByUsuarioClave(Integer userDocumento, String userClave) {
		for (Usuario usuario: usuarioList) {
			if (usuario.getDni().equals(userDocumento) && usuario.getClave().equals(userClave)){
				return usuario;
			}
		}
		
		return null;
	}
	//_______________Busca por nombre_________________________________
	public static Usuario getByUsuarioNombre(String userNombre) {
		for (Usuario usuario: usuarioList) {
			if (usuario.getNombre().equals(userNombre)){
				return usuario;
			}
		}
		return null;
	}
    //_________________________________________________________________
	public static int getIndex(Integer id) {
		for (int i = 0; i < usuarioList.size(); i++) {
			Usuario usuario = usuarioList.get(i);
			if (usuario.getUsuarioId() == id){
				return i;
			}
		}
		return -1;
	}
	//_________________________________________________________________
	public static int getById(Integer id) {
		for (int i = 0; i < usuarioList.size(); i++) {
			Usuario usuario = usuarioList.get(i);
			if (usuario.getUsuarioId() == id){
				return i;
			}
		}
		return -1;
	}
	//_________________________________________________________________
	public static void actualizar(Usuario usuario) {
		usuarioList.set(getIndex(usuario.getUsuarioId()), usuario);	
		
	}
   //__________________________________________________________________	
	public static void guardar(Usuario usuario){
		usuarioList.add(usuario);
		for (Usuario usuario1: usuarioList) {
			System.out.println(usuario1.getNombre());
		}
	}
	//__________________________________________________________________
	public static void eliminar(Usuario usuario) {
		usuarioList.remove(usuario);
	}	
}
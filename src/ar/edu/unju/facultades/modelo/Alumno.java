package ar.edu.unju.facultades.modelo;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
@ManagedBean
@SessionScoped
public class Alumno {
	private Integer usuarioId;
	private String nombre;
	private String apellido;
	private String domicilio;
	private Integer dni;
	private String eMail;
	
	public Alumno(Integer usuarioId, String nombre, String apellido, String domicilio, Integer dni, String eMail) {
		super();
		this.usuarioId = usuarioId;
		this.nombre = nombre;
		this.apellido = apellido;
		this.domicilio = domicilio;
		this.dni = dni;
		this.eMail = eMail;
	}
	public Alumno()
	{
		
	}
	public Integer getUsuarioId() {
		return usuarioId;
	}
	public void setUsuarioId(Integer usuarioId) {
		this.usuarioId = usuarioId;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getDomicilio() {
		return domicilio;
	}
	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}
	public Integer getDni() {
		return dni;
	}
	public void setDni(Integer dni) {
		this.dni = dni;
	}
	public String geteMail() {
		return eMail;
	}
	public void seteMail(String eMail) {
		this.eMail = eMail;
	}
	@Override
	public String toString() {
		return "Cliente [usuarioId=" + usuarioId + ", nombre=" + nombre + ", apellido=" + apellido + ", domicilio="
				+ domicilio + ", dni=" + dni + ", eMail=" + eMail + "]";
	}

	
	
}

package ar.edu.unju.facultades.beas;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import ar.edu.unju.facultades.manager.ManagerAlumno;
import ar.edu.unju.facultades.modelo.Alumno;
@ManagedBean
@SessionScoped
public class AlumnoBean {
	private String nombreC;
	private Integer documentoC;
	private boolean editar = false;
    private List<Alumno>clienteList;
	private Alumno cliente = new Alumno();
	private Alumno clienteNuevo = new Alumno();
//_______________________________________________
	public String getNombre() {
		return nombreC;
	}
	public void setNombre(String nombre) {
		this.nombreC = nombre;
	}
	public Integer getDocumento() {
		return documentoC;
	}
	public void setDocumento(Integer documento) {
		this.documentoC = documento;
	}
	public boolean isEditar() {
		return editar;
	}
	public void setEditar(boolean editar) {
		this.editar = editar;
	}
	public List<Alumno> getClienteList() {
		return clienteList;
	}
	public void setClienteList(List<Alumno> clienteList) {
		this.clienteList = clienteList;
	}
	public Alumno getCliente() {
		return cliente;
	}
	public void setCliente(Alumno cliente) {
		this.cliente = cliente;
	}
	public Alumno getClienteNuevo() {
		return clienteNuevo;
	}
	public void setClienteNuevo(Alumno clienteNuevo) {
		this.clienteNuevo = clienteNuevo;
	}
//________________Buscar___________________
			public String buscarCliente(){
				System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
				clienteList = ManagerAlumno.buscarCliente(nombreC, documentoC);		
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Buscando Clientes...", "Se realiza la busqueda de alumno");
		        FacesContext.getCurrentInstance().addMessage(null, message);	
		        System.out.println("Busqueda terminada");
				return null; 		
			}
//________________Guardar___________________			
			public void guardar(){
				System.out.println("...... guardando....");
				//lista.add(product);  Guardo el objeto en la lista est�tica		
				ManagerAlumno.guardar(cliente);		
				RequestContext.getCurrentInstance().execute("PF('modificarCliente1').hide();");
				clienteList = ManagerAlumno.list();
			}
//___________________Agregar_________________
			public void agregar(){
				System.out.println("...... agregando....");
				ManagerAlumno.guardar(clienteNuevo);  //Guardo el objeto en la lista est�tica
				System.out.println(clienteNuevo);
				RequestContext.getCurrentInstance().execute("PF('agregarCliente').hide();");
				clienteList = ManagerAlumno.list();
			}		
//_______carga la tabla cuando se realiza el login___________________		
			public String gestionAlumno(){
				clienteList = ManagerAlumno.list();
				return "gestionAlumnos";
			}
//_____________________________________________________________________________________			
			public void eliminar(){
				System.out.println("...... eliminando...." + cliente.getDni());
				//lista.add(product);  Guardo el objeto en la lista est�tica		
				ManagerAlumno.eliminar(cliente);		
//				RequestContext.getCurrentInstance().execute("PF('eliminarCliente').hide();");
				clienteList = ManagerAlumno.list();
			}			
			
}
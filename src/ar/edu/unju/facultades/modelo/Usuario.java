package ar.edu.unju.facultades.modelo;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
@ManagedBean
@SessionScoped
public class Usuario {
	private Integer usuarioId;
	private String nombre;
	private String clave;
	private Integer dni;
	private String eMail;	
	public Usuario(Integer usuarioId, String nombre, String clave, Integer dni, String eMail) {
		super();
		this.usuarioId = usuarioId;
		this.nombre = nombre;
		this.clave = clave;
		this.dni = dni;
		this.eMail = eMail;
	}
	
	public Usuario() {
	}
	
	public Integer getUsuarioId() {
		return usuarioId;
	}
	public void setUsuarioId(Integer usuarioId) {
		this.usuarioId = usuarioId;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public Integer getDni() {
		return dni;
	}
	public void setDni(Integer dni) {
		this.dni = dni;
	}
	public String geteMail() {
		return eMail;
	}
	public void seteMail(String eMail) {
		this.eMail = eMail;
	}
	@Override
	public String toString() {
		return "Usuario Id:" + usuarioId + ", Nombre:" + nombre
				+ ", clave:" + clave + ", Dni:" + dni + ", eMail:" + eMail;
	}	
}
package ar.edu.unju.facultades.beas;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.primefaces.context.RequestContext;
import ar.edu.unju.facultades.manager.ManagerUsuario;
import ar.edu.unju.facultades.modelo.Usuario;
@ManagedBean	
@SessionScoped
public class UsuarioBean {
	private Integer userDocumento = null;
	private String userClave = null;
	private String userNombre;
	private String nombre;
	private Integer documento;
	private boolean editar = false;
	
	private List<Usuario>usuarioList;

	private Usuario usuario = new Usuario();
	
	private Usuario usuarioNuevo = new Usuario();

//_________________________________________
	public Usuario getUsuarioNuevo() {
		return usuarioNuevo;
	}
	public void setUsuarioNuevo(Usuario usuarioNuevo) {
		this.usuarioNuevo = usuarioNuevo;
	}
	
	public List<Usuario> getUsuarioList() {
		return usuarioList;
	}
	public void setUsuarioList(List<Usuario> usuarioList) {
		this.usuarioList = usuarioList;
	}
	public Integer getUserDocumento() {
		return userDocumento;
	}
	public void setUserDocumento(Integer userDocumento) {
		this.userDocumento = userDocumento;
	}
	public String getUserClave() {
		return userClave;
	}
	public void setUserClave(String userClave) {
		this.userClave = userClave;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public boolean isEditar() {
		return editar;
	}
	public void setEditar(boolean editar) {
		this.editar = editar;
	}
	public String getUserNombre() {
		return userNombre;
	}
	public void setUserNombre(String userNombre) {
		this.userNombre = userNombre;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Integer getDocumento() {
		return documento;
	}
	public void setDocumento(Integer documento) {
		this.documento = documento;
	}
//________________Buscar___________________
		public String buscarNombre(){
			usuarioList = ManagerUsuario.buscarNombre(nombre, documento);		
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Buscando Usuarios...", "Se realiz� la b�squeda de Usuarios");
	        FacesContext.getCurrentInstance().addMessage(null, message);	
	        System.out.println("Busqueda terminada");
			return null; 		
		}
//__________________________________________
	public String validar(){		
		System.out.println(userDocumento);
		System.out.println(userClave);
		usuario = ManagerUsuario.getByUsuarioClave(userDocumento, userClave);
		if (usuario != null){
			editar = false;
			usuarioList = ManagerUsuario.list();
			return "home";		
		}else{
			FacesContext facesContext = FacesContext.getCurrentInstance();
			facesContext.addMessage("login", new FacesMessage("Los datos son invalidos"));
			return null;
		}	
	}
//________________primefaces___________________
	public String validarp(){		
		System.out.println(userDocumento);
		System.out.println(userClave);
		
		usuario = ManagerUsuario.getByUsuarioClave(userDocumento, userClave);
		if (usuario != null){
			editar = false;
			usuarioList = ManagerUsuario.list();
		
			return "home";		
		}else{
			FacesContext facesContext = FacesContext.getCurrentInstance();
			facesContext.addMessage("login", new FacesMessage("Los datos son invalidos"));
			return null;
		}	
	}
	//_____________________________________________________________________________________
	public String salir(){
		return "index";
	}
	//_____________________________________________________________________________________
	public String salirp(){
		return "indexp";
	}
	//_____________________________________________________________________________________
	public void misDatos(ActionEvent event){
		System.out.println(event.getComponent().getId());	
	    UIComponent sibling = event.getComponent().findComponent("idUsuario");
	    System.out.println(sibling.getAttributes().get("value").toString());
		editar = true;
	}
	//_____________________________________________________________________________________
	public void guardar(){
		System.out.println("...... guardando....");
		//lista.add(product);  Guardo el objeto en la lista est�tica		
		ManagerUsuario.guardar(usuario);		
		RequestContext.getCurrentInstance().execute("PF('modificarUsuario').hide();");
		usuarioList = ManagerUsuario.list();
	}
	//_____________________________________________________________________________________
	public void agregar(){
		System.out.println("...... agregando....");
		ManagerUsuario.guardar(usuarioNuevo);  //Guardo el objeto en la lista est�tica
		System.out.println(usuarioNuevo);
//		usuarioList = ManagerUsuario.list();
		RequestContext.getCurrentInstance().execute("PF('agregarUsuario').hide();");
		usuarioList = ManagerUsuario.list();
	}
	//_____________________________________________________________________________________
	public void eliminar(){
		System.out.println("...... eliminando....");
		//lista.add(product);  Guardo el objeto en la lista est�tica		
		ManagerUsuario.eliminar(usuario);		
//		RequestContext.getCurrentInstance().execute("PF('eliminarUsuario').hide();");
		usuarioList = ManagerUsuario.list();
	}
}